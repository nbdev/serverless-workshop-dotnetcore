#!/bin/bash

# install dotnet core
# from https://docs.aws.amazon.com/cloud9/latest/user-guide/sample-dotnetcore.html
sudo yum -y update
sudo yum -y install libunwind
wget https://download.microsoft.com/download/E/8/A/E8AF2EE0-5DDA-4420-A395-D1A50EEFD83E/dotnet-sdk-2.1.401-linux-x64.tar.gz
mkdir -p $HOME/dotnet
tar zxf dotnet-sdk-2.1.401-linux-x64.tar.gz -C $HOME/dotnet

# update $PATH in .bash_profile
sed -i '/PATH=/c\PATH=$PATH:$HOME/.local/bin:$HOME/bin:$HOME/dotnet' /home/ec2-user/.bash_profile

# install serverless framework
# from https://serverless.com/framework/docs/providers/aws/guide/installation/
npm install -g serverless

# cleanup
rm dotnet-sdk-2.1.401-linux-x64.tar.gz
rm ../README.md