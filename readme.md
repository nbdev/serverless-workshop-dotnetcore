# user setup

- Log in to your AWS account

- ensure you are in the 'N. Virginia' (us-east-1) region

- navigate to your Cloud9 IDE

- Clone workshop respository
```
git clone https://bitbucket.org/nbdev/serverless-workshop-dotnetcore.git
```

- Run `setup.sh`
```
cd serverless-workshop-dotnetcore
./setup.sh
```

- Open a new terminal tab

![new term tab](https://bitbucket.org/nbdev/serverless-workshop-dotnetcore/raw/3214e1b6beb02b3d8433d423a4ac143c21a814ab/img/setup1.png)

- Switch to the branch for the first step of the workshop
```
cd serverless-workshop-dotnetcore #you have to do this again, becuase you are in a new terminal
git checkout 0-hello_world_console_app
```