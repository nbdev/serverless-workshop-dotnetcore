#!/bin/bash

for user in `seq 1 $1`
do
    echo deleting user$user
    aws cloudformation delete-stack --stack-name user$user
done