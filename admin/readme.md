# Account Setup


`account_setup.sh` will automate the provisioning of users, passwords, and Cloud9 environments for workshop attendees.  Follow the steps below to provision your workshop environment:

- create a new AWS account
- login to AWS account with an IAM user with admin permissions (Onica users can use thier sso lab for these first two steps)
- navigate to Cloud9 in N. Virginia/us-east 1 region https://console.aws.amazon.com/cloud9/home?region=us-east-1
- create a cloud9 environment called 'admin', accept all other defaults
- in your cloud9 environment, do:


```
git clone https://bitbucket.org/nbdev/serverless-workshop-dotnetcore.git
cd serverless-workshop-dotnetcore/admin
./account_setup.sh 20 # replace '20' with the actual number of attendees expected in your workshop.  Consider over-provisionining in case others show up
```

- navigate to CloudFormation console in N. Virginia/us-east 1 region https://console.aws.amazon.com/cloudformation/home?region=us-east-1
- select each user (e.g. user1, user2). password will be shown on the `output` tab for each user
- navigate to https://console.aws.amazon.com/iam/home?region=us-east-1#/home and copy the 'IAM users sign-in link:', such as: https://123453336718.signin.aws.amazon.com/console
- provide each workshop attendee with sign-in link, username, and password
- direct attendees to follow the instructions in `readme.md` in the root dir of the master branch of this repo.