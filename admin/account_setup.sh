#!/bin/bash

getStackStatus() {
	aws cloudformation describe-stacks \
		--stack-name $1 \
		--query Stacks[].StackStatus \
		--output text
}

waitForState() {
	local status

	status=$(getStackStatus $1)

	while [[ "$status" != "$2" ]]; do
		echo "Waiting for stack $1 to obtain status $2 - Current status: $status"

		# If the status is not one of the "_IN_PROGRESS" status' then consider
		# this an error
		if [[ "$status" != *"_IN_PROGRESS"* ]]; then
			exitWithErrorMessage "Unexpected status '$status'"
		fi

		status=$(getStackStatus $1)

		sleep 5
	done
	echo "Stack $1 obtained $2 status"
}


# main
aws cloudformation create-stack --stack-name gen-passwords --capabilities CAPABILITY_IAM --template-body file://gen_passwords.yml
waitForState gen-passwords CREATE_COMPLETE

for user in `seq 1 $1`
do
    echo provisioning user$user
    aws cloudformation create-stack --stack-name user$user --capabilities CAPABILITY_NAMED_IAM --template-body file://gen_users.yml
done
